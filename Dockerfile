FROM debian:sid

RUN apt-get update -y -qq && apt-get install -y -qq nsis wget p7zip-full

RUN cd /tmp && mkdir registry && cd registry && wget http://nsis.sourceforge.net/mediawiki/images/4/47/Registry.zip && 7z x Registry.zip && cd /usr/share/nsis && cp -a /tmp/registry/Desktop/Include/* Include/ && cp -a /tmp/registry/Desktop/Plugin/* Plugins/x86-ansi/ && rm -rf /tmp/registry
